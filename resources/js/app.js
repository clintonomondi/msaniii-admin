import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueTelInput from 'vue-tel-input';
import Notifications from 'vue-notification';
import vueCountryRegionSelect from 'vue-country-region-select';
import JsonExcel from "vue-json-excel";
import moment from 'moment'
import VueGeolocation from 'vue-browser-geolocation';
import JwPagination from 'jw-vue-pagination';
import VueTimeago from 'vue-timeago'

import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';



Vue.use(VueToast);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueTelInput);
Vue.use(Notifications);
Vue.use(vueCountryRegionSelect);
Vue.component("downloadExcel", JsonExcel);
Vue.use(VueGeolocation);
Vue.component('jw-pagination', JwPagination);
Vue.use(VueTimeago, {
    name: 'Timeago', // Component name, `Timeago` by default
    locale: 'en', // Default locale
    // We use `date-fns` under the hood
    // So you can use all locales from it
    // locales: {
    //     'zh-CN': require('date-fns/locale/zh_cn'),
    //     ja: require('date-fns/locale/ja')
    // }
});

import App from './view/App'
import Login from './pages/login'
import Home from './pages/home'
import Artists from './artist/artists'
import Users from './artist/users'
import Password from './pages/password'
import Forget from './pages/forget'
import Artist from './artist/artist'
import Deductions from './settings/deductions'
import Other from './settings/other'
import Mpesa from './report/mpesa'
import Withdrawal from './report/withdrawal'



const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'login',
            component: Login,
            meta: { hideNavigation: true }
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: { hideNavigation: true }
        },
        {
            path: '/home',
            name: 'home',
            component: Home,
        },
        {
            path: '/artists',
            name: 'artists',
            component: Artists,
        },
        {
            path: '/users',
            name: 'users',
            component: Users,
        },
        {
            path: '/password',
            name: 'password',
            component: Password,
        },
        {
            path: '/password/forget',
            name: 'forget',
            component: Forget,
            meta: { hideNavigation: true }
        },
        {
            path: '/artist/:id',
            name: 'artist',
            component: Artist,
        },
        {
            path: '/settings/deductions',
            name: 'deductions',
            component: Deductions,
        },
        {
            path: '/settings/other',
            name: 'other',
            component: Other,
        },
        {
            path: '/report/mpesa',
            name: 'mpesa',
            component: Mpesa,
        },
        {
            path: '/report/withdrawal',
            name: 'withdrawal',
            component: Withdrawal,
        },





    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,

});

